//
//  IPageSequenceCallback.swift
//  core
//
//  Created by Admin on 18/04/22.
//

import Foundation


public protocol IPageSequenceCallback {
    func onPageSequenceSuccess(object:NSObject?)
    func onPageSequenceIntermediate()
    func onPageSequenceFailure(message:String?)
}
