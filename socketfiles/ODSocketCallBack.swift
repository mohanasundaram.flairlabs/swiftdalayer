//
//  ODSocketCallBack.swift
//  core
//
//  Created by Admin on 18/04/22.
//

import Foundation

public protocol ODSocketCallBack {
    func onConnected(roomId: String, participantId: String)
    func setScreenShotUpload(artifact: String, link: String)
    func onDisconnect(code: String)
    func onCompleted(message: String)
    func onTouch(x:Int,y:Int)
}

