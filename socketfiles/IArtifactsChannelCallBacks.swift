//
//  IArtifactsChannelCallBacks.swift
//  core
//
//  Created by Admin on 18/04/22.
//

import Foundation

public protocol IArtifactsChannel {
    func onArtifactSuccess(object:NSObject)
    func onArtifactFailure(message:String)
}

