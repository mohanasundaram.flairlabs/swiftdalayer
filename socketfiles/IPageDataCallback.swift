//
//  IPageDataCallback.swift
//  core
//
//  Created by Admin on 18/04/22.
//

import Foundation

public protocol IPageDataCallback {
    func onPageDataSuccess(page:String,object:NSObject)
    func onPageDataFailure(message:String)
}

