//
//  MSSocketCallBack.swift
//  core
//
//  Created by Admin on 18/04/22.
//

import Foundation

public protocol MSSocketCallBack {
    func setRoomCall(roomCall: NSObject)
    func onDisconnect(envelope: NSObject)
    func cleanUp()
    func cleanUpProvider()
    func invokeMediaFinishedCallback(obj: NSObject)
    func msSocketStateErrorCallback(error: String, message: String, canReconnect: Bool)
    func onSocketDisconnected()
    func onSocketOpen()
}
