//
//  InstructionView.swift
//  app
//
//  Created by Admin on 06/09/21.
//

import UIKit
import Lottie
import Foundation

public enum RetryOptions {
    case SessionContinue
    case SessionOverride
}
public protocol InstructionActionDelegate {
    func didSetRetryButton(screen: RetryOptions)
}
public class InstructionView: UIView {
    
    @IBOutlet weak var contentView: UIView!
    @IBOutlet weak var imageView: UIView!
    @IBOutlet weak var messageHeader: UILabel!
    @IBOutlet weak var messageInfo: UILabel!
    @IBOutlet weak var informationOne: UILabel!
    @IBOutlet weak var informationTwo: UILabel!
    @IBOutlet weak var btnRetry: UIButton!
    @IBOutlet weak var backgroundInfoViewOne: UIView!
    @IBOutlet weak var backgroundInfoViewTwo: UIView!
    @IBOutlet weak var backgroundInfoViewThree: UIView!
    
    public var delegate: InstructionActionDelegate?
    
    let kCONTENT_XIB_NAME           =   "InstructionView"
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        commonInit()
    }
    
    required init?(coder: NSCoder) {
        super.init(coder: coder)
        commonInit()
    }
    private func commonInit() {
        Bundle(for: type(of: self)).loadNibNamed(kCONTENT_XIB_NAME, owner: self, options: nil)
        contentView.fixInView(self)
        
    }
    public func loadNetworkCheckInfoView() {
        self.updateColours()
        self.imageView.removeSubviews()
        let animationView = AnimationView(name: "wifi_loading", bundle: Bundle(for: type(of: self)), imageProvider: nil, animationCache: nil)
        animationView.frame = CGRect(x: 0, y: 0, width: imageView.frame.width, height: imageView.frame.height)
        animationView.contentMode = .scaleAspectFill
        animationView.loopMode = .loop
        
        imageView.addSubview(animationView)
        
        animationView.play()
        self.messageHeader.text = StringHandler.Wait.rawValue
        self.messageInfo.text = StringHandler.CheckingNetwork.rawValue
        self.informationOne.text = StringHandler.InstructionOne.rawValue
        self.informationTwo.text = StringHandler.InstructionTwo.rawValue
        btnRetry.isHidden = true
        self.informationOne.isHidden = false
        self.informationTwo.isHidden = false
    }
    public func loadConnectingToAgentView() {
        self.updateColours()
        self.imageView.removeSubviews()
        let animationView = AnimationView(name: "agent_connecting", bundle: Bundle(for: type(of: self)), imageProvider: nil, animationCache: nil)
        animationView.frame = CGRect(x: 0, y: 0, width: imageView.frame.width, height: imageView.frame.height)
        animationView.contentMode = .scaleAspectFill
        animationView.loopMode = .loop
        animationView.play()
        imageView.addSubview(animationView)
        
        self.messageHeader.text = StringHandler.Connecting.rawValue
        self.messageInfo.text = StringHandler.AcceptingCall.rawValue
        self.informationOne.text = StringHandler.InstructionOne.rawValue
        self.informationTwo.text = StringHandler.InstructionTwo.rawValue
        self.btnRetry.isHidden = true
        self.informationOne.isHidden = false
        self.informationTwo.isHidden = false
    }
    public func loadConnectingView(message: String) {
        self.updateColours()
        self.imageView.removeSubviews()
        let animationView = AnimationView(name: "connecting", bundle: Bundle(for: type(of: self)), imageProvider: nil, animationCache: nil)
        animationView.frame = CGRect(x: 0, y: 0, width: imageView.frame.width, height: imageView.frame.height)
        animationView.contentMode = .scaleAspectFill
        animationView.loopMode = .loop
        animationView.play()
        imageView.addSubview(animationView)
        self.messageHeader.text = message
        self.messageInfo.text = StringHandler.Refresh.rawValue
        self.informationOne.isHidden = true
        self.informationTwo.isHidden = true
        backgroundInfoViewThree.isHidden = true
        self.btnRetry.isHidden = true
    }
    public func loadScreenShotRequestView(title: String,message:String) {
        self.updateColours()
        self.imageView.removeSubviews()
        let animationView = AnimationView(name: "connecting", bundle: Bundle(for: type(of: self)), imageProvider: nil, animationCache: nil)
        animationView.frame = CGRect(x: 0, y: 0, width: imageView.frame.width, height: imageView.frame.height)
        animationView.contentMode = .scaleAspectFill
        animationView.loopMode = .loop
        animationView.play()
        imageView.addSubview(animationView)
        self.messageHeader.text = title
        self.messageInfo.text = message
        self.informationOne.isHidden = true
        self.informationTwo.isHidden = true
        backgroundInfoViewThree.isHidden = true
        self.btnRetry.isHidden = true
    }
    public func loadAgentNotAvailableView() {
        self.updateColours()
        contentView.backgroundColor = hexStringToUIColor(hex: ThemeConfig.shared.getSecondaryMainColor())
        self.imageView.removeSubviews()
        let imageView = UIImageView(frame: CGRect(x: 0, y: 0, width: 60, height: 40))
        imageView.SetLogoImage(ThemeConfig.shared.getLogo())
        self.imageView.addSubview(imageView)
        self.messageHeader.text = StringHandler.Unavailable.rawValue
        self.messageInfo.text = StringHandler.TryAfterSometimes.rawValue
        self.informationOne.isHidden = true
        self.informationTwo.isHidden = true
        btnRetry.isHidden = false
        btnRetry.tag = 0
        backgroundInfoViewThree.isHidden = true
        
    }
    public func loadNetworkDisconnectedStatusView() {
        self.updateColours()
        contentView.backgroundColor = hexStringToUIColor(hex: ThemeConfig.shared.getSecondaryMainColor())
        self.imageView.removeSubviews()
        let animationView = AnimationView(name: "no_internet", bundle: Bundle(for: type(of: self)), imageProvider: nil, animationCache: nil)
        animationView.frame = CGRect(x: 0, y: 0, width: imageView.frame.width, height: imageView.frame.height)
        animationView.contentMode = .scaleAspectFill
        animationView.loopMode = .loop
        animationView.play()
        imageView.addSubview(animationView)
        self.messageHeader.text = StringHandler.Disconnected.rawValue
        self.messageInfo.text = StringHandler.GoodNetwork.rawValue
        self.informationOne.isHidden = true
        self.informationTwo.isHidden = true
        btnRetry.isHidden = false
        btnRetry.tag = 1
        backgroundInfoViewOne.backgroundColor = .none
        backgroundInfoViewTwo.backgroundColor = .none
        backgroundInfoViewThree.isHidden = true
    }
    public func loadLocationFailureView() {
        self.updateColours()
        contentView.backgroundColor = hexStringToUIColor(hex: ThemeConfig.shared.getSecondaryMainColor())
        self.imageView.removeSubviews()
        let imageView = UIImageView(frame: CGRect(x: 0, y: 0, width: 60, height: 60))
        if let myImage = UIImage(named: "NetworkDisconnected") {
            let tintableImage = myImage.withRenderingMode(.alwaysTemplate)
            imageView.image = tintableImage
        }
        imageView.tintColor = .red
        self.imageView.addSubview(imageView)
        self.messageHeader.text = "Location out of bounds"
        self.messageInfo.text = "Location should be within India"
        self.informationOne.isHidden = true
        self.informationTwo.isHidden = true
        btnRetry.isHidden = false
        btnRetry.tag = 1
        backgroundInfoViewOne.backgroundColor = .none
        backgroundInfoViewTwo.backgroundColor = .none
        backgroundInfoViewThree.isHidden = true
        
    }
    
    public func loadKycCompletedStatusView() {
        self.updateColours()
        contentView.backgroundColor = hexStringToUIColor(hex: ThemeConfig.shared.getSecondaryMainColor())
        self.imageView.removeSubviews()
        let imageView = UIImageView(frame: CGRect(x: 0, y: 0, width: 60, height: 40))
        imageView.SetLogoImage(ThemeConfig.shared.getLogo())
        self.imageView.addSubview(imageView)
        self.messageHeader.text = StringHandler.ThankYou.rawValue
        self.messageInfo.text = ""
        self.informationOne.isHidden = true
        self.informationTwo.isHidden = true
        btnRetry.isHidden = true
        backgroundInfoViewTwo.backgroundColor = .clear
        backgroundInfoViewThree.isHidden = true
        backgroundInfoViewOne.isHidden = true

    }
    public func loadKycStatusView(status:String) {
        self.updateColours()
        contentView.backgroundColor = hexStringToUIColor(hex: ThemeConfig.shared.getSecondaryMainColor())
        self.imageView.removeSubviews()
        let imageView = UIImageView(frame: CGRect(x: 0, y: 0, width: 60, height: 40))
        imageView.SetLogoImage(ThemeConfig.shared.getLogo())
        self.imageView.addSubview(imageView)
        self.messageHeader.text = getStatus(status: status).title
        self.messageInfo.text = getStatus(status: status).message
        self.informationOne.isHidden = true
        self.informationTwo.isHidden = true
        btnRetry.isHidden = true
        backgroundInfoViewTwo.backgroundColor = .clear
        backgroundInfoViewThree.isHidden = true
        backgroundInfoViewOne.isHidden = true

    }
    public func loadHealthCheckFailureView() {
        self.updateColours()
        contentView.backgroundColor = hexStringToUIColor(hex: ThemeConfig.shared.getSecondaryMainColor())
        self.imageView.removeSubviews()
        let imageView = UIImageView(frame: CGRect(x: 0, y: 0, width: 60, height: 40))
        imageView.SetLogoImage(ThemeConfig.shared.getLogo())
        self.imageView.addSubview(imageView)
        self.messageHeader.text = StringHandler.BadNetwork.rawValue
        self.messageInfo.text = StringHandler.InitiateGoodNetwork.rawValue
        self.informationOne.isHidden = true
        self.informationTwo.isHidden = true
        btnRetry.isHidden = false
        btnRetry.tag = 1
        backgroundInfoViewThree.isHidden = true
        
    }
    @IBAction public func didTapButton(_ sender: UIButton) {
        if btnRetry.tag == 0 {
            self.delegate?.didSetRetryButton(screen: .SessionContinue)
        } else {
            self.delegate?.didSetRetryButton(screen: .SessionOverride)
        }
    }
    private func updateColours() {
        messageHeader.textColor = hexStringToUIColor(hex: ThemeConfig.shared.getSecondaryContrastColor())
        messageInfo.textColor = hexStringToUIColor(hex: ThemeConfig.shared.getSecondaryContrastColor())
        btnRetry.backgroundColor = hexStringToUIColor(hex: ThemeConfig.shared.getPrimaryMainColor())
        backgroundInfoViewTwo.backgroundColor = hexStringToUIColor(hex: ThemeConfig.shared.getSecondaryMainColor())
    }
}
