//
//  DalCaptureFactory.swift
//  core
//
//  Created by FL Mohan on 05/05/22.
//

import Foundation


public class DalCaptureFactory {

    public init() {
        
    }

    public func getDalCapture(dalConfig:DalConfig) -> DALCapture {
        let dalComponent = DalComponentFactory.sharedInstance.getDalComponent(dalConfig: dalConfig)
        return dalComponent.getDalCapture()
    }
}
