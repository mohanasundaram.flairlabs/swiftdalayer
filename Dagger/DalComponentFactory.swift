//
//  DalComponentFactory.swift
//  core
//
//  Created by FL Mohan on 05/05/22.
//

import Foundation
import Cleanse

public class DalComponentFactory {

    private static var privateSharedInstance: DalComponentFactory?

    private static var dalComponent:DalComponent.Root?

    public static var sharedInstance: DalComponentFactory = {

        if privateSharedInstance    == nil {
            privateSharedInstance = DalComponentFactory()
        }

        return privateSharedInstance!
    }()
    public init() {

    }
    public func getDalComponent(dalConfig:DalConfig) -> DalComponent.Root {
        if DalComponentFactory.dalComponent == nil {
            DalComponentFactory.dalComponent =  try! ComponentFactory.of(DalComponent.self).build(DalFeed(dalConfig:dalConfig))
        }
        return DalComponentFactory.dalComponent!
    }
    public func getDalComponent() -> DalComponent.Root {
        if DalComponentFactory.dalComponent == nil {
            print("DalComponent not initialized please call getDalComponent(dalConfig:DalConfig)")
        }
        return DalComponentFactory.dalComponent!
    }
    public static func destroyInstance() {
        privateSharedInstance   =   nil
        dalComponent = nil
    }
}
