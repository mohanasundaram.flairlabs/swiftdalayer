//
//  DalComponent.swift
//  captureapp
//
//  Created by Admin on 29/03/22.
//

import Foundation


import Foundation
import Cleanse
import core

//public protocol DalComponent  {
//
//    func getDalCapture() -> DALCapture
//
//    func getLoggerWrapper() -> LoggerWrapper
//
//}
//public class dalComponent:DalComponent {
//    init() {
//    }
//    public func getDalCapture() -> DALCapture {
//        return DALCapture()
//    }
//    public func getLoggerWrapper() -> LoggerWrapper {
//        return LoggerWrapper()
//    }
//
//
//}
//public struct getDalComponent: Component {
//
//    public typealias Root = DalComponent
//
//    public static func configure(binder: SingletonBinder) {
//        binder.include(module: DalModule.self)
//    }
//
//    public static func configureRoot(binder bind: ReceiptBinder<DalComponent>) -> BindingReceipt<DalComponent> {
//        return bind.to(factory: dalComponent.init)
//    }
//}
//public struct DalComponent {
//    let dalCapture: DALCapture
//    let loggerWrapper: LoggerWrapper
//    func getDalCapture() -> DALCapture {
//        return dalCapture
//    }
//    func getLoggerWrapper() -> LoggerWrapper {
//        return loggerWrapper
//    }
//}
//public struct CoreComponent : RootComponent {
//    public typealias Root = DalComponent
//    public static func configureRoot(binder bind: ReceiptBinder<DalComponent>) -> BindingReceipt<DalComponent> {
//        return bind.to(factory: DalComponent.init)
//    }
//    public static func configure(binder: SingletonBinder) {
//        // "include" the modules that create the component
//        binder.include(module: DalModule.self)
//        // bind our root Object
//        binder
//            .bind(DalComponent.self)
//            .to(factory: DalComponent.init)
//
//        binder.install(dependency: CoreComponent.self)
//    }
//}
//public struct Root : RootComponent {
//    public typealias Root = CoreComponent
//
//    public static func configureRoot(binder bind: ReceiptBinder<CoreComponent>) -> BindingReceipt<CoreComponent> {
//        return bind.to(factory: CoreComponent.init)
//    }
//    public static func configure(binder: Binder<Unscoped>) {
//        binder.install(dependency: CoreComponent.self)
//    }
//    // ...
//}
public protocol DalProtocol {
    func getDalCapture() -> DALCapture
    func getLoggerWrapper() -> LoggerWrapper
}
public struct DalStructure:DalProtocol {
    let dalCapture:DALCapture
    
    public func getDalCapture() -> DALCapture {
        return dalCapture
    }
    public func getLoggerWrapper() -> LoggerWrapper {
        return LoggerWrapper()
    }
}
public struct DalComponent : Cleanse.RootComponent {
    public typealias Root = DalStructure
    public typealias Seed = DalFeed

    public static func configure(binder: SingletonBinder) {
        binder.include(module: DalModule.self)
    }

    public static func configureRoot(binder bind: ReceiptBinder<Root>) -> BindingReceipt<Root> {
        return bind.to { (dd:Seed) in
            Root.init(dalCapture:DALCapture(pgLogger: Logger(), dalConfig: dd.dalConfig, captureSocket: CaptureSocket()))
        }
    }
}
public struct DalFeed {
    let dalConfig:DalConfig

    public init(dalConfig:DalConfig) {
        self.dalConfig = dalConfig
    }
}
