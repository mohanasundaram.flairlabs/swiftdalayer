//
//  DalConfig.swift
//  svc library
//
//  Created by Admin on 01/04/22.
//

import Foundation

public class DalConfig {

    private var token:String = ""
    private var loggerUrl:String = ""
    private var enableLogging:Bool = false
    private var captureStatusUrl:String = ""
    private var captureSocketUrl:String = ""
    
    public init() {}
    private init(configBuilder:DalConfigBuilder) {
        self.token = configBuilder.token
        self.loggerUrl = configBuilder.loggerUrl
        self.enableLogging = configBuilder.enableLogging
        self.captureStatusUrl = configBuilder.captureStatusUrl
        self.captureSocketUrl = configBuilder.captureSocketUrl
    }
    public func getToken() -> String {
        return self.token
    }
    public func getLoggerUrl() -> String {
        return self.loggerUrl
    }
    public func getEnableLogging() -> Bool {
        return self.enableLogging
    }
    public func getCaptureStatusUrl() -> String {
        return self.captureStatusUrl
    }
    public func getCaptureSocketUrl() -> String {
        return self.captureSocketUrl
    }
    public class DalConfigBuilder {
        private(set) var token: String = ""
        private(set) var loggerUrl: String = ""
        private(set) var enableLogging: Bool = true
        private(set) var captureStatusUrl: String = "/captures/status"
        private(set) var captureSocketUrl: String = "/socket/capture/websocket?"

        public init() {}

        public func setToken(token: String) -> DalConfigBuilder {
            self.token = token
            return self
        }
        public func setLoggerUrl(loggerUrl: String) -> DalConfigBuilder {
            self.loggerUrl = loggerUrl
            return self
        }
        public func enableLogging(enableLogging: Bool) -> DalConfigBuilder {
            self.enableLogging = enableLogging
            return self
        }
        public func setCaptureSocketUrl(captureSocketUrl: String) -> DalConfigBuilder {
            self.captureSocketUrl = captureSocketUrl + self.captureSocketUrl
            return self
        }
        public func setCaptureServerUrl(serverUrl: String) -> DalConfigBuilder {
            self.captureStatusUrl = serverUrl + self.captureStatusUrl
            return self
        }

        public func build() -> DalConfig {
            let dalConfig = DalConfig(configBuilder: self)
            return dalConfig
        }
    }
}
