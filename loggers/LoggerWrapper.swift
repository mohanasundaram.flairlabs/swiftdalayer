//
//  LoggerWrapper.swift
//  app
//
//  Created by Admin on 30/11/21.
//

import Foundation

public class LoggerWrapper: NSObject {
    
    var ENABLE_LOGGING: Bool = true
    var loggers: [String: Logger] = [:]

    public func createLogger(key: String, token: String, loggerUrl: String) -> Logger {
        return createLogger(key: key, token: token, loggerUrl: loggerUrl, enableLogging: true, isRetryEnabled: true)
    }
    
    public func createLogger(key: String, token: String, loggerUrl: String,enableLogging:Bool,isRetryEnabled:Bool) -> Logger {
        if loggers.contains(key: key) {
            print(" Logger cannot be invoked multiple times!")
            return loggers[key]!
        } else {
            let loggerInstance = Logger()

            loggerInstance.createInstance(token: token, loggerUrl: loggerUrl, enableLogging: enableLogging, isRetryEnabled: isRetryEnabled)
            
            var defaultLog = ModalLogDetails()
            defaultLog.service_category = "captureSDK"
            defaultLog.app_vsn = "1.1"
            defaultLog.log_version = "V1"
            
            loggerInstance.setDefaults(defaultLogDetails: defaultLog)
            
            var meta: [String: Any] = [:]
            meta.updateValue(token, forKey: "capture_token")
            loggerInstance.setMetaDefaults(metaDefaults: meta)
            
            loggers.updateValue(loggerInstance, forKey: key)
            return loggerInstance

        }

    }
    public func log(logLevel: String, logDetails: ModalLogDetails, meta: [String: Any]) {
        if !loggers.contains(key: "capture") || !ENABLE_LOGGING {
            return
        }
        loggers["capture"]?.log(logLevel: logLevel, logDetails: logDetails, meta: meta)
        print(loggers)
    }
    
    // add param: meta?: any
    public func logPageRender(loggerStartTime: Double, component: String,referenceId:String,referenceType:String, meta: [String: Any]) {
        loggers["capture"]?.logPageRender(loggerStartTime: loggerStartTime, component: component, referenceId: referenceId, referenceType: referenceType, meta: meta)
    }
    
    // set default can be used within the mentioned parameteters only, provide the key and the modal data to set
    // default value for the required key
    public func setDefault(key: String, defaultLog: ModalLogDetails) {
        loggers[key]?.setDefaults(defaultLogDetails: defaultLog)
    }
    public func setMetaDefaults(key: String, metaDefaults: [String: Any]) {
        loggers[key]?.setMetaDefaults(metaDefaults: metaDefaults)
    }
    public func clearLoggers() {
        loggers.removeAll()
    }
    public func removeLogger(key: String) {
        loggers.removeValue(forKey: key)
    }
    public func getLoggers(key: String) -> Logger? {
        return loggers[key]
    }
}
