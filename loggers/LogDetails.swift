//
//  LogDetails.swift
//  app
//
//  Created by Admin on 29/11/21.
//

import Foundation

public struct LogDetails {
    var serviceCategory: String? = "serviceCategory"
    var service: String?  = "service"
    var event_type: String?  = "event_type"
    var event_name: String?  = "event_name"
    var timestamp: String?  = "timestamp"
    var component: String?  = "component"
    var eventSource: String?  = "event_source"
    var logger_session_id: String?  = "logger_session_id"
    var exceptionName: String? = "exceptionName"
    var exceptionDescription: String?  = "exceptionDescription"
    var referenceID: String?  = "referenceID"
    var referenceType: String?  = "referenceType"
    var log_version: String?  = "log_version"
    var app_vsn: String?  = "app_vsn"

    var liveMonitoring: String? = "liveMonitoring"; // boolean
    var publish_to_dlk: String? = "publish_to_dlk"; // boolean
}
