//
//  ModelEvent.swift
//  capture
//
//  Created by Admin on 21/04/22.
//

import Foundation

public class ModelEvent {
    var event:String = ""
    var key:String = ""
    var jsonObject:NSObject?

    public init() {

    }
    public init(event:String,key:String) {
        self.event = event
        self.key = key
    }
    public init(event:String,key:String,jsonObject:NSObject) {
        self.event = event
        self.key = key
        self.jsonObject = jsonObject
    }
    public init(event:String,jsonObject:NSObject) {
        self.event = event
        self.jsonObject = jsonObject
    }

    public func getEvent() -> String {
        return event
    }

    public func setEvent(event:String) {
        self.event = event
    }

    public func getJsonObject() -> NSObject {
        return jsonObject ?? [] as NSObject
    }

    public func setJsonObject(jsonObject:NSObject) {
        self.jsonObject = jsonObject
    }

    public func getKey() -> String {
        return key
    }

    public func setKey(key:String) {
        self.key = key
    }
}

