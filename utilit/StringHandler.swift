//
//  StringHandler.swift
//  app
//
//  Created by Admin on 23/11/21.
//

import Foundation

public enum StringHandler: String {
    case Wait
    case CheckingNetwork
    case InstructionOne
    case InstructionTwo
    case Connecting
    case AcceptingCall
    case Reconnecting
    case ConnectingOnly
    case Refresh
    case Unavailable
    case TryAfterSometimes
    case Disconnected
    case GoodNetwork
    case ThankYou
    case BadNetwork
    case InitiateGoodNetwork
    case Warning
    case EndCall
    case Yes
    case No
    case Cancel
    case Ok
    
    public var rawValue: String {
        switch self {
        case .Wait : return LocalizableStrings.Wait
        case .CheckingNetwork : return LocalizableStrings.CheckingNetwork
        case .InstructionOne: return LocalizableStrings.InstructionOne
        case .InstructionTwo: return LocalizableStrings.InstructionTwo
        case .Connecting: return LocalizableStrings.Connecting
        case .AcceptingCall: return LocalizableStrings.AcceptingCall
        case .Reconnecting: return LocalizableStrings.Reconnecting
        case .ConnectingOnly: return LocalizableStrings.ConnectingOnly
        case .Refresh: return LocalizableStrings.Refresh
        case .Unavailable: return LocalizableStrings.Unavailable
        case .TryAfterSometimes: return LocalizableStrings.TryAfterSometimes
        case .Disconnected: return LocalizableStrings.Disconnected
        case .GoodNetwork: return LocalizableStrings.GoodNetwork
        case .ThankYou : return LocalizableStrings.ThankYou
        case .BadNetwork: return LocalizableStrings.BadNetwork
        case .InitiateGoodNetwork: return LocalizableStrings.InitiateGoodNetwork
        case .Warning: return LocalizableStrings.Warning
        case .EndCall: return LocalizableStrings.EndCall
        case .Ok: return LocalizableStrings.Ok
        case .Yes: return LocalizableStrings.Yes
        case .Cancel : return LocalizableStrings.Cancel
        case .No: return LocalizableStrings.No
        }
    }
}

public struct LocalizableStrings {
    static let Wait  = NSLocalizedString("Please Wait...", comment: "")
    static let CheckingNetwork  = NSLocalizedString("We're checking your network connection. This won't take long.", comment: "")
    static let InstructionOne  = NSLocalizedString("1.Do NOT press back or leave this window", comment: "")
    static let InstructionTwo  = NSLocalizedString("2.Do Not let your screen sleep Doing so will remove you from the queue", comment: "")
    static let Connecting  = NSLocalizedString("You will be connected to the Representative shortly", comment: "")
    static let AcceptingCall  = NSLocalizedString("Please wait on this screen while the representative accepts your call", comment: "")
    static let Reconnecting  = NSLocalizedString("Reconnecting...", comment: "")
    static let ConnectingOnly = NSLocalizedString("Connecting...", comment: "")
    static let Refresh  = NSLocalizedString("Please do not refresh or press the back button", comment: "")
    static let Unavailable  = NSLocalizedString("Representative is Currently Unavailable", comment: "")
    static let TryAfterSometimes  = NSLocalizedString("Please try again after some time", comment: "")
    static let Disconnected  = NSLocalizedString("Network Disconnected", comment: "")
    static let GoodNetwork  = NSLocalizedString("Please ensure you are connected to a good network, and click RETRY", comment: "")
    static let ThankYou  = NSLocalizedString("Thank You", comment: "")
    static let BadNetwork  = NSLocalizedString("It looks like you're not in a good network. There is a high chance that your call might get disconnected due to bad network.", comment: "")
    static let InitiateGoodNetwork  = NSLocalizedString("Please initiate your verification from a good network and try again.", comment: "")
    static let Warning  = NSLocalizedString("Warning!", comment: "")
    static let EndCall  = NSLocalizedString("Are you sure you want to end the video call?", comment: "")
    static let Ok  = NSLocalizedString("Ok", comment: "")
    static let Yes  = NSLocalizedString("Yes", comment: "")
    static let Cancel  = NSLocalizedString("Cancel", comment: "")
    static let No  = NSLocalizedString("No", comment: "")
}
