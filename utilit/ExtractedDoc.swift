//
//  ExtractedDoc.swift
//  Hostapp
//
//  Created by Admin on 07/03/22.
//

import Foundation

public class ExtractedDoc {
    var taskKey:String = ""
    var taskType:String = ""

    public init(taskType:String,taskKey:String) {
        self.taskKey = taskKey
        self.taskKey = taskKey
    }
    public func getTaskKey() -> String {
        return taskKey
    }
    public func setTaskKey(taskKey:String) {
        self.taskKey = taskKey
    }
    public func getTaskType() -> String {
        return taskType
    }
    public func setTaskType(taskType:String) {
        self.taskType = taskType
    }
}
