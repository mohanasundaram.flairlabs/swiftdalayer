////
////  SreverCommunicationHandler.swift
////  app
////
////  Created by Admin on 18/08/21.
////
//
//import Foundation
//import Alamofire
//
//class ApiHandler {
//
//    class func getRequest(url: String, header: HTTPHeaders?, completion: @escaping(_ onSessionSuccess: NSObject?, _ onSessionFailure: NSObject?) -> Void) {
//        Session.customSession.request(URL(string: url)!, method: .get, headers: header).responseString { (response)  in
//            
//            if let headerFields = response.response?.allHeaderFields as? [String: String], let URL = response.request?.url {
//                let cookies = HTTPCookie.cookies(withResponseHeaderFields: headerFields, for: URL)
//                print(cookies)
//            }
//            switch response.result {
//            
//            case .success:
//                
//                print("Server Request = \(String(describing: response.request)) and Server Response = \(String(describing: String(data: response.data ?? Data(), encoding: .utf8)))")
//                
//                guard let responseData = response.data else {  return   }
//                
//                do {
//                    let dictionary = try JSONSerialization.jsonObject(with: responseData, options: .fragmentsAllowed) as! NSObject
//                    completion(dictionary, nil)
//                    
//                } catch {
//                    completion(nil, error as NSObject)
//                }
//                break
//            case .failure:
//                print("Server Request = \(String(describing: response.request)) and Server Response = \(String(describing: String(data: response.data ?? Data(), encoding: .utf8)))")
//                guard let responseData = response.data else {  return   }
//                
//                do {
//                    let dictionary = try JSONSerialization.jsonObject(with: responseData, options: .fragmentsAllowed) as! NSObject
//                    completion(nil, dictionary)
//                    
//                } catch {
//                    completion(nil, error as NSObject)
//                }
//                break
//                
//            }
//        }
//    }
//}
//extension Session {
//    
//    public static let customSession: Session = {
//        let configuration = URLSessionConfiguration.default
//        configuration.timeoutIntervalForRequest = 1000
//        return Session(configuration: configuration)
//    }()
//}
